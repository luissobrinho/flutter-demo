import 'package:flutter/material.dart';
import 'package:demos/pages/home/home_page.dart';

void main() => runApp(
      new MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: HomePage(title: 'Flutter Demo'),
      ),
    );
