import 'package:flutter/material.dart';

class BotoesPage extends StatefulWidget {
  BotoesPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  BotoesPageState createState() => BotoesPageState();

  static void router(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (BuildContext context) => BotoesPage(title: 'Botões Page'),
      ),
    );
  }
}

class BotoesPageState extends State<BotoesPage> {
  ShapeBorder _shapeBorder;
  @override
  Widget build(BuildContext context) {
    final ButtonThemeData buttonTheme =
        ButtonTheme.of(context).copyWith(shape: _shapeBorder);

    return Scaffold(
      body: DefaultTabController(
        length: 5,
        child: Scaffold(
          appBar: AppBar(
            title: Text(widget.title),
            bottom: TabBar(
              tabs: [
                Tab(text: 'Raised'),
                Tab(text: 'Flat'),
                Tab(text: 'Outline'),
                Tab(text: 'Icon'),
                Tab(text: 'Action'),
              ],
            ),
          ),
          body: TabBarView(children: [
            ButtonTheme.fromButtonThemeData(
              data: buttonTheme,
              child: buildRaiseButton(
                  'Reeaise Button adiciona dimesão a layouts geralment simples. '
                  'E enfatizam funções em espaços ocuandos ou amplos'),
            ),
            ButtonTheme.fromButtonThemeData(
              data: buttonTheme,
              child: buildFlatButton(
                  'Flat Button é um botão mais "Clean". Recomandado para utilizar em barras de ferramentas e nas coixas de dialogs.'),
            ),
            ButtonTheme.fromButtonThemeData(
              data: buttonTheme,
              child: buildOutlineButton(
                  'Outline Button são opacos e elevam-se quando pressionados. Ele são frequentemente emparelados com butões em relevo para indicar opções secundárias'),
            ),
            ButtonTheme.fromButtonThemeData(
              data: buttonTheme,
              child: buildIcoButton(),
            ),
            ButtonTheme.fromButtonThemeData(
              data: buttonTheme,
              child: buildActioButton(),
            )
          ]),
        ),
      ),
    );
  }

  Widget buildRaiseButton(title) {
    return Align(
      alignment: Alignment(0.0, -0.2),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            title,
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          ButtonBar(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              RaisedButton(
                child: Text('REAISED BUTTON'),
                onPressed: () {},
              ),
              RaisedButton(
                onPressed: null,
                child: Text('DESABLITADO'),
              )
            ],
          ),
          ButtonBar(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              RaisedButton(
                child: Text('REAISED BUTTON'),
                onPressed: () {},
                color: Colors.blue,
              ),
              RaisedButton(
                onPressed: null,
                child: Text('DESABLITADO'),
                color: Colors.amber,
              )
            ],
          ),
          ButtonBar(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              RaisedButton.icon(
                onPressed: () {},
                icon: Icon(
                  Icons.add,
                  size: 18.8,
                ),
                label: Text('REAISED BUTTON'),
              ),
              RaisedButton.icon(
                onPressed: null,
                icon: Icon(
                  Icons.add,
                  size: 18.8,
                ),
                label: Text('DESABLITADO'),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget buildFlatButton(title) {
    return Align(
      alignment: Alignment(0.0, -0.2),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ButtonBar(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              FlatButton(
                child: Text('FLAT BUTTON'),
                onPressed: () {},
              ),
              FlatButton(
                onPressed: null,
                child: Text('DESABLITADO'),
              )
            ],
          ),
          ButtonBar(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              FlatButton(
                child: Text(
                  'FLAT BUTTON',
                  style: TextStyle(color: Colors.red),
                ),
                onPressed: () {},
              ),
              FlatButton(
                onPressed: null,
                child: Text('DESABLITADO'),
              )
            ],
          ),
          ButtonBar(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              FlatButton.icon(
                onPressed: () {},
                icon: Icon(
                  Icons.add_circle_outline,
                  size: 18.8,
                ),
                label: Text('FLAT BUTTON'),
              ),
              FlatButton.icon(
                onPressed: null,
                icon: Icon(
                  Icons.add_circle_outline,
                  size: 18.8,
                ),
                label: Text('DESABLITADO'),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget buildOutlineButton(title) {
    return Align(
      alignment: Alignment(0.0, -0.2),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            title,
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          ButtonBar(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              OutlineButton(
                child: Text('OUTLINE BUTTON'),
                onPressed: () {},
              ),
              OutlineButton(
                onPressed: null,
                child: Text('DESABLITADO'),
              )
            ],
          ),
          ButtonBar(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              OutlineButton(
                child: Text(
                  'OUTLINE BUTTON',
                  style: TextStyle(color: Colors.red),
                ),
                onPressed: () {},
              ),
              OutlineButton(
                onPressed: null,
                child: Text('DESABLITADO'),
              )
            ],
          ),
          ButtonBar(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              OutlineButton.icon(
                onPressed: () {},
                icon: Icon(
                  Icons.add,
                  size: 18.8,
                ),
                label: Text('OUTLINE BUTTON'),
              ),
              OutlineButton.icon(
                onPressed: null,
                icon: Icon(
                  Icons.add,
                  size: 18.8,
                ),
                label: Text('DESABLITADO'),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget buildIcoButton() {
    return Align(
      alignment: Alignment(0.0, -0.2),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ButtonBar(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.thumb_up),
                onPressed: () {},
              ),
              IconButton(
                onPressed: null,
                icon: Icon(Icons.thumb_up),
              )
            ],
          ),
          ButtonBar(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.thumb_down),
                color: Colors.red,
                onPressed: () {},
              ),
              IconButton(
                icon: Icon(Icons.thumb_down),
                onPressed: null,
              )
            ],
          )
        ],
      ),
    );
  }

  Widget buildActioButton() {
    return Align(
      alignment: Alignment(0.0, -0.2),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ButtonBar(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              FloatingActionButton(
                child: Icon(Icons.add),
                onPressed: () {},
              ),
              FloatingActionButton(
                child: Icon(Icons.add),
                onPressed: null,
              )
            ],
          ),
          ButtonBar(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              FloatingActionButton(
                child: Icon(Icons.add),
                backgroundColor: Colors.red,
                onPressed: () {},
              ),
              FloatingActionButton(
                child: Icon(Icons.add),
                backgroundColor: Colors.red,
                onPressed: null,
              )
            ],
          ),
        ],
      ),
    );
  }
}
