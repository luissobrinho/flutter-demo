import 'package:flutter/material.dart';

class OperacoesMatematicas extends StatefulWidget {
  @override
  _OperacoesMatematicasState createState() => _OperacoesMatematicasState();

  static void router(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (BuildContext context) => OperacoesMatematicas(),
      ),
    );
  }
}

class _OperacoesMatematicasState extends State<OperacoesMatematicas> {
  double numero1 = 0.0;

  double numero2 = 0.0;

  @override
  Widget build(BuildContext context) {
    TextField firstNumberField = TextField(
      decoration: InputDecoration(labelText: 'First Number'),
      keyboardType: TextInputType.number,
      onChanged: (value) {
        try {
          numero1 = double.parse(value);
        } catch (ex) {
          numero1 = 0;
        }
      },
    );

    TextField secondNumberField = TextField(
      decoration: InputDecoration(labelText: 'Second Number'),
      keyboardType: TextInputType.number,
      onChanged: (value) {
        try {
          numero2 = double.parse(value);
        } catch (ex) {
          numero2 = 0;
        }
      },
    );

    RaisedButton button = RaisedButton(
      onPressed: () {
        double add = numero1 + numero2;
        double sub = numero1 - numero2;

        AlertDialog resultDialog = AlertDialog(
          content: Text('add = $add & sub = $sub'),
          title: Text('Results'),
          actions: <Widget>[
            FlatButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        );
        showDialog(
            context: context, child: resultDialog, barrierDismissible: false);
      },
      child: Text('Calc'),
    );

    Container container = Container(
      padding: EdgeInsets.all(20.0),
      child: Column(
        children: <Widget>[firstNumberField, secondNumberField, button],
      ),
    );

    AppBar appBar = AppBar(
      title: Text('Operações Matimáticas'),
    );

    Scaffold scaffold = Scaffold(
      appBar: appBar,
      body: container,
    );

    return scaffold;
  }
}
