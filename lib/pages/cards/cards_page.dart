import 'package:flutter/material.dart';
import 'package:demos/pages/cards/card_item_class.dart';
import 'package:demos/pages/cards/card_viagem_class.dart';

class CardsPage extends StatefulWidget {
  CardsPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  CardsPageState createState() => CardsPageState();

  static void router(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (BuildContext context) => CardsPage(
          title: 'Cards Page',
        ),
      ),
    );
  }
}

class CardsPageState extends State<CardsPage> {
  ShapeBorder _shape;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[100],
      appBar: AppBar(
        title: Text(widget.title),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.border_style),
            onPressed: () {
              setState(() {
                _shape = _shape != null
                    ? null
                    : RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(16.0),
                          topRight: Radius.circular(16.0),
                          bottomLeft: Radius.circular(16.0),
                          bottomRight: Radius.circular(16.0),
                        ),
                      );
              });
            },
          )
        ],
      ),
      body: ListView(
        itemExtent: CardItens.height,
        padding: EdgeInsets.only(top: 8.0, right: 8.0, bottom: 8.0, left: 8.0),
        children: destinosList.map((CardViagem cards) => Container(
            margin: EdgeInsets.only(bottom: 0.0),
            child: CardItens(
              destinos: cards,
              shape: _shape,
            ),
          )
        ).toList(),
      ),
    );
  }
}
