class CardViagem {
  const CardViagem({this.title, this.description});

  final String title;
  final List<String> description;
}

final List<CardViagem> destinosList = <CardViagem>[
  new CardViagem(
    title: 'Rio deJaneiro',
    description: <String>['Pão de Açucar', 'Cristo Redentor', 'Marancanã'],
  ),
  new CardViagem(
    title: 'São Paulo',
    description: <String>['Avenida Paulista', 'Morambi', 'Mercadão'],
  ),
  new CardViagem(
    title: 'Maceió',
    description: <String>['Ponta Verde', 'Pajuçara', 'Cruzeiro do Sul'],
  ),
];