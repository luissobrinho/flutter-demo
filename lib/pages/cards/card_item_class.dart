import 'package:flutter/material.dart';
import 'package:demos/pages/cards/card_viagem_class.dart';

class CardItens extends StatelessWidget {
  CardItens({Key key, @required this.destinos, this.shape}) : super(key: key);
  static const double height = 150.0;
  final CardViagem destinos;
  final ShapeBorder shape;

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final TextStyle descricaoStyle = theme.textTheme.subhead;

    return SafeArea(
      top: false,
      bottom: false,
      child: Container(
        padding: EdgeInsets.only(top: 1.0, right: 8.0, bottom: 1.0, left: 8.0),
        height: height,
        child: Card(
          shape: shape,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0.0),
                child: Text(
                  destinos.title,
                  style: descricaoStyle.copyWith(color: Colors.redAccent, fontSize: 20.0),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0.0),
                  child: DefaultTextStyle(
                    softWrap: false,
                    overflow: TextOverflow.ellipsis,
                    style: descricaoStyle,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(bottom: 8.0),
                          child: Text(
                            destinos.description[0],
                            style: descricaoStyle.copyWith(color: Colors.green),
                          ),
                        ),
                        Text(destinos.description[1]),
                        Text(destinos.description[2])
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
