import 'package:flutter/material.dart';
import 'package:demos/enums/tipo_lista_enum.dart';

class ListasPage extends StatefulWidget {
  ListasPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  ListasPageState createState() => ListasPageState();

  static void router(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (BuildContext context) => ListasPage(
          title: 'Listas Page',
        ),
      ),
    );
  }
}

class ListasPageState extends State<ListasPage> {
  static final GlobalKey<ScaffoldState> scaffoldKey =
      GlobalKey<ScaffoldState>();
  PersistentBottomSheetController<Null> bottomSheet;
  PersistentBottomSheetController<Null> _bottomSheet;
  MaterialListeType _itemType = MaterialListeType.tresLinhas;
  bool _ordenacaoRecersa = false;
  bool _showAvatars = true;
  bool _showIcons = false;
  List<String> items = <String>[
    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
    'G',
    'H',
    'I',
    'J',
    'K',
    'L',
    'M',
    'N',
    'O',
    'P',
    'Q',
    'R',
    'S',
    'T',
    'U',
    'V',
    'W',
    'X',
    'Y',
    'Z'
  ];

  void changeItemType(MaterialListeType type) {
    setState(() {
      _itemType = type;
    });
    _bottomSheet?.setState(() {
      Navigator.of(context).pop();
    });
  }

  void _showConfigurationSheet() {
    print(scaffoldKey.currentState.toString());
    bottomSheet = scaffoldKey.currentState.showBottomSheet((_) {
      return Builder(builder: (BuildContext buttonSheetContext) {
        return Container(
          decoration: BoxDecoration(
            border: Border(top: BorderSide(color: Colors.black26)),
          ),
          child: ListView(
            shrinkWrap: true,
            primary: false,
            children: <Widget>[
              MergeSemantics(
                child: ListTile(
                  dense: true,
                  title: Text('Uma Linha'),
                  trailing: Radio<MaterialListeType>(
                    groupValue: _itemType,
                    onChanged: changeItemType,
                    value: MaterialListeType.umaLinha,
                  ),
                ),
              ),
              MergeSemantics(
                child: ListTile(
                  dense: true,
                  title: Text('Duas Linhas'),
                  trailing: Radio<MaterialListeType>(
                    groupValue: _itemType,
                    onChanged: changeItemType,
                    value: MaterialListeType.duasLinas,
                  ),
                ),
              ),
              MergeSemantics(
                child: ListTile(
                  dense: true,
                  title: Text('Três Linhas'),
                  trailing: Radio<MaterialListeType>(
                    groupValue: _itemType,
                    onChanged: changeItemType,
                    value: MaterialListeType.tresLinhas,
                  ),
                ),
              ),
              MergeSemantics(
                child: ListTile(
                  dense: true,
                  title: Text('Show Avatar'),
                  trailing: Checkbox(
                    value: _showAvatars,
                    onChanged: (bool value) {
                      setState(() {
                        _showAvatars = value;
                      });
                      _bottomSheet.setState(() {});
                    },
                  ),
                ),
              ),
              MergeSemantics(
                child: ListTile(
                  dense: true,
                  title: Text('Show Icons'),
                  trailing: Checkbox(
                    value: _showIcons,
                    onChanged: (bool value) {
                      setState(() {
                        _showIcons = value;
                      });
                      _bottomSheet.setState(() {});
                    },
                  ),
                ),
              ),
            ],
          ),
        );
      });
    });

    setState(() {
      _bottomSheet = bottomSheet;
    });

    _bottomSheet.closed.whenComplete(() {
      if (mounted) {
        setState(() {
          _bottomSheet = null;
        });
      }
    });
  }

  Widget buidlListTile(BuildContext context, String item) {
    Widget secondary;
    if (_itemType == MaterialListeType.duasLinas) {
      secondary = Text('Texto Secundário');
    } else if (_itemType == MaterialListeType.tresLinhas) {
      secondary = Text(
          'Essa linha adional aparece com o modulo 3 linhas. Vou adicionar mais linhas aqui pra quebrar o texto na tela');
    }

    return MergeSemantics(
      child: ListTile(
        isThreeLine: _itemType == MaterialListeType.tresLinhas,
        leading: _showAvatars
            ? ExcludeSemantics(
                child: CircleAvatar(
                  child: Text(item),
                ),
              )
            : null,
        title: Text('Este item representa a letra $item.'),
        subtitle: secondary,
        trailing: _showIcons
            ? Icon(
                Icons.info,
                color: Theme.of(context).disabledColor,
              )
            : null,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Iterable<Widget> listTiles =
        items.map((String item) => buidlListTile(context, item));

    listTiles = ListTile.divideTiles(tiles: listTiles, context: context);

    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text(widget.title),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.sort_by_alpha),
            tooltip: 'Sort',
            onPressed: () {
              setState(() {
                _ordenacaoRecersa = !_ordenacaoRecersa;
                items.sort((String a, String b) =>
                    _ordenacaoRecersa ? b.compareTo(a) : a.compareTo(b));
              });
            },
          ),
          IconButton(
            icon: Icon(Icons.more_vert),
            tooltip: 'More',
            onPressed: _bottomSheet == null ? _showConfigurationSheet : null,
          )
        ],
      ),
      body: Scrollbar(
        child: ListView(
          padding: EdgeInsets.symmetric(vertical: 8.0),
          children: listTiles.toList(),
        ),
      ),
    );
  }
}
