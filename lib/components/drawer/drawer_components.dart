import 'package:demos/pages/matematica/matematica_page.dart';
import 'package:flutter/material.dart';
import 'package:demos/pages/botoes/botoes_page.dart';
import 'package:demos/pages/listas/listas_page.dart';
import 'package:demos/pages/cards/cards_page.dart';

class DraweComponent extends StatefulWidget {
  DraweComponent({Key key, this.title}) : super(key: key);
  final String title;

  @override
  DraweComponentState createState() => DraweComponentState();
}

class DraweComponentState extends State<DraweComponent> {
  String photo = 'lib/assets/img/user/photo.jpg';
  String backgound = 'lib/assets/img/background/bg.jpg';
  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(
      children: <Widget>[
        UserAccountsDrawerHeader(
          accountName: Text('Luis Eduardo Altino'),
          accountEmail: Text('ads.luis.sobrinho@gmail.com'),
          currentAccountPicture: GestureDetector(
            onTap: () => print('object'),
            child: CircleAvatar(
              backgroundImage: AssetImage(photo),
            ),
          ),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(backgound),
              fit: BoxFit.fill,
            ),
          ),
        ),
        ListTile(
          title: Text('Botões'),
          trailing: Icon(Icons.arrow_forward),
          onTap: () {
            Navigator.of(context).pop();
            BotoesPage.router(context);
          },
        ),
        ListTile(
          title: Text('Listas'),
          trailing: Icon(Icons.arrow_forward),
          onTap: () {
            Navigator.of(context).pop();
            ListasPage.router(context);
          },
        ),
        ListTile(
            title: Text('Cards'),
            trailing: Icon(Icons.arrow_forward),
            onTap: () {
              Navigator.of(context).pop();
              CardsPage.router(context);
            }),
        Divider(),
        ListTile(
          title: Text('Matemática'),
          trailing: Icon(Icons.arrow_forward),
          onTap: () {
            Navigator.of(context).pop();
            OperacoesMatematicas.router(context);
          },
        ),
        ListTile(
          title: Text('Cancelar'),
          trailing: Icon(Icons.close),
          onTap: () {
            Navigator.of(context).pop();
          },
        ),
      ],
    ));
  }
}
